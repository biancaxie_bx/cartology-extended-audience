WITH 
	
	time_period AS ( 
	SELECT CAST('2020-01-07' AS DATE) AS start_pw_end_date
	)
  , sales AS (
	SELECT pw_end_date
	, crn
	, SUM(NVL(tot_amt_excld_gst_wo_wow,0)) AS sales
	, COUNT(DISTINCT basket_key) AS basket
	FROM loyalty_bi_analytics.bi_article_sales AS bas
  CROSS JOIN time_period
	WHERE LENGTH(TRIM(bas.crn))>3
	AND pw_end_date >= start_pw_end_date - 3*7
	GROUP BY 1,2)

  SELECT pw_end_date, crn
  , SUM(CASE WHEN pw_end_date BETWEEN pw_end_date - 3*7 AND pw_end_date THEN sales ELSE 0 END) AS sales_l4
  , SUM(CASE WHEN pw_end_date BETWEEN pw_end_date - 3*7 AND pw_end_date THEN basket ELSE 0 END) AS basket_l4

  ;

  SELECT * FROM loyalty_bi_analytics.bi_article_sales LIMIT 10